#pragma once


#include <QtCore>
#include <QtSql>
#include <QtConcurrent>

#include "../../Common/Plugin/plugin_base.h"
#include "../../Interfaces/i_async_database.h"

#define CONNECTION_FAILED -1

typedef QList<QVector<QVariant>> QueryValuesReturnType;
typedef QPair<int, QueryValuesReturnType> QueryReturnType;
class Query : public QObject, public IQuery
{
	Q_OBJECT
public:
	Query(QWeakPointer<QThreadPool> pool, const QString &query);
	Query(QWeakPointer<QThreadPool> pool, const QString &query, const QList<QString> &valuePlaceholders, const QList<QVariant> &values);
	~Query() {}

	// IQueryCallback interface
public:
	QUuid uuid() override;
	QObject *object() override;
	int lastInsertId() override;
	QueryValuesReturnType values() override;

public slots:
	void exec() override;
	void onFinished();

signals:
	void completed(QUuid uuid) override;

private:
	QWeakPointer<QThreadPool> m_pool;
	QString m_queryStr;
	QList<QString> m_valuePlaceholders;
	QList<QVariant> m_values;

	QFuture<QueryReturnType> m_future;
	QFutureWatcher<QueryReturnType> m_futureWatcher;
	QueryReturnType m_result;
	QUuid m_uuid;
	QString m_password;
	QString m_connectionName;
	QThread m_thread;
};

class DatabaseConnection
{
	Q_DISABLE_COPY(DatabaseConnection)
public:
	DatabaseConnection()
		: m_name(QString::number(QRandomGenerator::global()->generate(), 36))
	{
		auto database = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), m_name);
		database.setDatabaseName("db_SQLITE.db");
		database.open();
	}

	~DatabaseConnection()
	{
		QSqlDatabase::removeDatabase(m_name);
	}

	QSqlDatabase database()
	{
		return QSqlDatabase::database(m_name);
	}

private:
	QString m_name;
};

//! \addtogroup DataBase_imp
//! \{
class DataBase : public QObject, public PluginBase, public IAsyncDataBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "TimeKeeper.DBModule.Test" FILE "PluginMeta.json")
	Q_INTERFACES(
	        IPlugin
	        IAsyncDataBase
	)

public:
	DataBase();
	virtual ~DataBase() override;

	// PluginBase interface
public:
	virtual void onReady() override;

	// IAsyncDataBase interface
public:
	QSharedPointer<IQuery> executeQueryAsync(const QString &queryStr) override;
	QSharedPointer<IQuery> executeQueryAsync(const QString &queryStr,
	        const QList<QString> &valuePlaceholders, const QList<QVariant> &values) override;

private slots:
	void onExecuted(QSqlQuery &query);

private:
	QSqlDatabase currentDatabase();

private:
	//! \brief possibleDriverNames
	//! Stores possible database drivers in list by it's priority.
	QSharedPointer<QThreadPool> m_pool;
	QList<QString> m_possibleDriverNames;
	QString m_password;
	QString m_connectionName;
	QSqlDatabase m_dbconn;
};
//! \}

